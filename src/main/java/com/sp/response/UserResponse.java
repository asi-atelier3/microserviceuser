package com.sp.response;

public class UserResponse {
    private String token;
    private Integer userId;
    public String getToken(){
        return token;
    }
    public Integer getUserId(){
        return userId;
    }
    public void setToken(String token){
        this.token = token;
    }
    public void setUserId(Integer userId){
        this.userId = userId;
    }
}
