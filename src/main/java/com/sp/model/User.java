package com.sp.model;


import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Entity;
import at.favre.lib.crypto.bcrypt.BCrypt;

@Entity
public class User {
	@Id
	@GeneratedValue
	private Integer id;
    private String username;
    private String mail;
    private String password;
    private Integer money;

    // default constructor
    public User() {
    }

    public User(Integer id, String username, String mail, String password, Integer money) {
        super();
        this.id = id;
        this.username = username;
        this.mail = mail;
        this.password = password;
        this.money = money;
    }

    public Integer getId() {
        return this.id;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMail() {
        return this.mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Integer getMoney() {
        return this.money;
    }

    public void setMoney(Integer money) {
        this.money = money;
    }
    
    @Override
    public String toString() {
        return "User [id=" + id + ", username=" + username + ", mail=" + mail + ", money=" + money + "]";
    }
}

