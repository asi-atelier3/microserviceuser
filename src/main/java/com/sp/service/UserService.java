package com.sp.service;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import at.favre.lib.crypto.bcrypt.BCrypt;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import com.sp.model.User;
import com.sp.repository.UserRepository;
import com.sp.response.UserResponse;

@Service
public class UserService {
	@Autowired
	UserRepository userRepository;
    
    @Value("${token.app.jwtSecret}")
    private String jwtSecret;
    @Value("${token.app.jwtExpirationMs}")
    private int jwtExpirationMs;



    public UserResponse login(User user) {
        String clearPassword = user.getPassword();
        user = userRepository.findByUsername(user.getUsername());
        if(user != null){
            if(BCrypt.verifyer().verify(clearPassword.toCharArray(), user.getPassword()).verified){
                UserResponse res = new UserResponse();
                res.setToken(Jwts.builder()
                    .claim("id", user.getId())
                    .claim("mail", user.getMail())
                    .claim("username", user.getUsername())
                    .claim("money", user.getMoney())
                    .setIssuedAt(new Date())
                    .setExpiration(new Date((new Date()).getTime() + jwtExpirationMs))
                    .signWith(SignatureAlgorithm.HS512, jwtSecret)
                    .compact());
                res.setUserId(user.getId());
                return res;

            }else{
                return null;
            }
        }else{
            return null;
        }
    }

    public ArrayList<User> getUsers() {
		ArrayList<User> users = new ArrayList<>();
		Iterable<User> itUser = userRepository.findAll();
		for (User user : itUser) {
			users.add(user);
		}
		return users;
	}



    public UserResponse addUser(User user) {
        try {
            user.setPassword(BCrypt.withDefaults().hashToString(12, user.getPassword().toCharArray()));
            User newUser = userRepository.save(user);
            UserResponse res = new UserResponse();
            res.setToken(Jwts.builder()
                    .claim("id", user.getId())
                    .claim("mail", user.getMail())
                    .claim("username", user.getUsername())
                    .claim("money", user.getMoney())
                    .setIssuedAt(new Date())
                    .setExpiration(new Date((new Date()).getTime() + jwtExpirationMs))
                    .signWith(SignatureAlgorithm.HS512, jwtSecret)
                    .compact());
            res.setUserId(newUser.getId());
            return res;
        } catch (Exception e) {
            return null;
        }
    }

    public User getUserById(int id) {
        return userRepository.findById(id).get();
    }

    public boolean deleteUser(int id) {
		try {
			userRepository.deleteById(id);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

    public User updateUser(int id, User user) {
        User oldUser = getUserById(id);
        oldUser.setUsername(user.getUsername());
        oldUser.setMail(user.getMail());
        oldUser.setMoney(user.getMoney());
        oldUser.setPassword(BCrypt.withDefaults().hashToString(12, user.getPassword().toCharArray()));
        return userRepository.save(oldUser);
    }
}
