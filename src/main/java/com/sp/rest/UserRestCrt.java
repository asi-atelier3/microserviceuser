package com.sp.rest;

import java.util.ArrayList;

import com.sp.model.User;
import com.sp.response.UserResponse;
import com.sp.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/api/users")
public class UserRestCrt {
   // Injecting the UserService class into the UserRestCrt class.
    @Autowired
    UserService userService;

    /*
    * GET /users
    */
    @RequestMapping(method=RequestMethod.GET,value="")
    public ArrayList<User> getUsers() {
        ArrayList<User> users=userService.getUsers();
        return users;
    }

    /*
     * POST /users/register
     */
    @RequestMapping(method=RequestMethod.POST,value="/register")
    public UserResponse addUser(@RequestBody User user) {
        return userService.addUser(user);
    }

    /*
     * GET /users/:uid
     */
    @RequestMapping(method=RequestMethod.GET,value="/{id}")
    public User getUser(@PathVariable String id) {
        User user = userService.getUserById(Integer.valueOf(id));
        return user;
    }

    /*
     * POST /users/login
     */
    @RequestMapping(method=RequestMethod.POST,value="/login")
    public UserResponse login(@RequestBody User user) {
        return userService.login(user);
    }

    /*
     * DELETE /users/:id
     */
    @RequestMapping(method=RequestMethod.DELETE,value="/{id}")
    public boolean deleteUser(@PathVariable String id) {
        return userService.deleteUser(Integer.valueOf(id));
    }

    /*
     * PUT /users/:id
     */
    @RequestMapping(method=RequestMethod.PUT,value="/{id}")
    public User updateUser(@PathVariable String id, @RequestBody User user) {
        return userService.updateUser(Integer.valueOf(id), user);
    }
}
