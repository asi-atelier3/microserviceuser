# Build
FROM maven:3.6.3-jdk-11 AS userservice-build
ENV USERSERVICE_HOME /opt/userservice
WORKDIR $USERSERVICE_HOME
COPY pom.xml .
RUN mvn dependency:go-offline

COPY src ./src
RUN mvn package -DskipTests

# Run
FROM openjdk:11-jre
ENV USERSERVICE_HOME /opt/userservice
WORKDIR $USERSERVICE_HOME
COPY --from=userservice-build $USERSERVICE_HOME/target/*.jar $USERSERVICE_HOME/userservice.jar

EXPOSE 8080

ENTRYPOINT java -jar userservice.jar